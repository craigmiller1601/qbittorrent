# QBitTorrent

This will deploy QBitTorrent to my Kubernetes cluster via Helm. It will also use nordvpn as a proxy to secure traffic.

## Configuration

All configuration is done internally by the chart. Any configuration changes done via the UI will not be persisted.